This repository contains the required files and documentation to reproduce the methods made during the project.
The full documentation and background is available in the document *Special Course Magnus Johansen* while the technical requirements are found below. 
## Scripts
Both an example of the FloPy model and the two implementations of GW in the optimization models (corresponding to Model A and Model B in the documentation) are located in the */Scripts/* folder.
Also a version of Modflow and the Ipopt-solver used in the optimization are found in the */Scripts/* folder, that are used in the examples provided. 
####  Create the conda environment named GW_project from the environment.yml file
The optimization runs from the Pyomo library in Python. 
The numerical GW model is computed using the FloPy library. 
The conda environment can be created by  navigating to the corresponding directory and using the command below from the Anaconda Prompt: 
*conda env create -f environment.yml*
## Data
All data was gathered from course 12333 at DTU Environment. The subcatchments were created using the MERIT dataset and zonal statistics were used to aggreagate data on the subcatchments files. 
All data required to run the optimization is found in */Data/*
## Output
Outputs of the models are saved in .xlsx format in */Output/*


