# -*- coding: utf-8 -*-
"""
These scripts are created as part of the special course "" by Magnus Johansen 

@author: Magnus Johansen
"""
from pyomo.environ import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import pdb
import sys 
import time
# def opt_model():

# start = time.time()
# model directory 


def compute_data_dict(data_df,C_idx,Catchments_IDs,years,area_df,unit):
    """
    Computes correct dictionary from a df input. 
    
    """
    df = data_df.iloc[:,-(years*12):]
    df_t = df.transpose()
    df_t.columns = C_idx


    df_unit_corrected = df_t.copy()

    if unit == 'mm/day_to_m3/month':
        for i in range(1,len(C_idx)+1):
            df_unit_corrected[i] = df_t[i].values *(10**-3*30) * area_df[i].values *10**-6  # mm/day * m/mm * day/month * m/month * m2 = m3/month
#     df_unit_corrected[Catchments_IDs]
    elif unit == 'mm/month_to_m3/month':
        for i in range(1,len(C_idx)+1):
            df_unit_corrected[i] = df_t[i].values *(10**-3) * area_df[i].values *10**-6 # mm/day * m/mm * day/month * m/month * m2 = m3/month


    # pdb.set_trace()
    dict_tmp = {}
    final_dict = {}
    for i,C_id in enumerate(Catchments_IDs):
        dict_tmp = dict(zip((idx[(i)*num_times:(i+1)*num_times]),df_unit_corrected[C_id]))
        final_dict = {**final_dict, **dict_tmp}
    return final_dict,df_unit_corrected
# compute_data_dict(Runoff_df_full,C_idx,Catchments_IDs,years,area_df,unit = 'mm/day')
# ((idx[(9)*num_times:(0)*num_times]),df_unit_corrected[9])[1].values
# =============================================================================
# ----------------------------------- Input -----------------------------------
# =============================================================================
general_input = pd.read_excel(r'../Data/General_input.xlsx')

# Define time index
sim_time = general_input['Simulation time[years]'][0]
num_times = int(sim_time*12)
ntimes = list(range(1,num_times+1))
ntimesp = list(range(1,num_times+2))
years =int(num_times/12) # number of years to compute

# Define zone/subcatchment index
Catchments_IDs = [9,10,11]
num_zones = len(Catchments_IDs) # number of zones
# nzones = Catchments_IDs 
nzones = list(range(1,num_zones+1))

# Create index from zones and time periods 
# Format is (zone,time)
idx=list(zip(np.repeat(nzones,len(ntimes)),np.tile(ntimes,len(nzones))))
# print(idx)

# Define a multiindex as in Excel 
index = pd.MultiIndex.from_tuples(zip(np.repeat(nzones,num_times),(ntimes)*num_zones))

#### -------------------------------- Area [m2] -------------------------------

# Area dataframe [m2]
area_full_df = pd.read_excel(r'../Data/Area.xlsx') # Unit mm/day
area_df_tmp = pd.DataFrame(area_full_df ['area'])
area_df= area_df_tmp.transpose()
C_idx = area_full_df['DN'].values # catchment index
area_df.columns = C_idx
# print(area_df) # units m2
total_area = area_df[Catchments_IDs].sum(axis= 1)
# print('total area is: {} m2 or {:.0f}*10^9 m2'.format(total_area.values[0],total_area.values[0]/(10**9)))

#### --------------------------- Runoff [mm/day] -------------------------------

# Real runoff - Compute runoff for the most recent timesteps in X years and write to the correct index. 

# Runoff # Unit mm/day
Runoff_full_df = pd.read_excel(r'../Data/GRUN_runoff.xlsx')
Runoff,Runoff_df = compute_data_dict(Runoff_full_df,C_idx,Catchments_IDs,years,area_df,unit = 'mm/day_to_m3/month')
# Runoff

#####    Runoff comparison between SubC

# df = Runoff_full_df.iloc[:,:]
# Runoff_full_df['DN']
# df.set_index(Runoff_full_df['DN'],inplace = True)
# plt.figure(figsize=(20,10))
# for i in Catchments_IDs:
#     plt.plot(df.iloc[i,3:],label = df.index[i])
# plt.legend()
# plt.show()
# df.mean(axis=1)

#### -----------------------Water use [mm/month] -------------------------------

# Domestic water consumption
cons_dom_full_df = pd.read_excel(r'../Data/cons_dom_corrected.xlsx')
Demand_domestic_cons,_ = compute_data_dict(cons_dom_full_df,C_idx,Catchments_IDs,years,area_df,unit = 'mm/month_to_m3/month')
Demand_domestic = Demand_domestic_cons 

# Irrigation water consumption
cons_irr_full_df = pd.read_excel(r'../Data/cons_irr_corrected.xlsx')
Demand_irrigation_cons,_ = compute_data_dict(cons_irr_full_df,C_idx,Catchments_IDs,years,area_df,unit = 'mm/month_to_m3/month')
Demand_irrigation = Demand_irrigation_cons 

##### ---------------- Curtailment costs and reservoir data -------------------

CC_dom = general_input['Curtailment cost domestic'][0]
CC_irr = general_input['Curtailment cost irrigation'][0]

CC_domestic = dict(zip(idx,np.repeat(CC_dom,num_zones*num_times)))
CC_irrigation = dict(zip(idx,np.repeat(CC_irr,num_zones*num_times)))


# Reservoirs and initial storage
Reservoir_capacity_tmp = general_input['Reservoir capacity [km3]'].tolist()

# Correction due to unit mistake 
Reservoir_capacity_tmp[10] = Reservoir_capacity_tmp[10] *1000
# The reservoir was computed to be 1000 times bigger than in realitity. 
# The initial storage was set to 0.003 so the volume availble was not too high. 
# However, in practice unlimited storage capacity was available.
# This should be fixed in the final model. 

# unit conversion: km3 -> million m3
Reservoir_capacity = [i*10**9/10**6 for  i in Reservoir_capacity_tmp]

inital_storage_fraction = 0.003
Initial_storage = [i*inital_storage_fraction for i in Reservoir_capacity]

# Create index that match the rest of the setup. 
Cap_tmp = [Reservoir_capacity[C] for C in Catchments_IDs]
Cap = dict(zip(nzones,Cap_tmp))

Sini_tmp = [Initial_storage[C] for C in Catchments_IDs]
Sini = dict(zip(nzones,Sini_tmp))


##### ------------------------- Ecosystem services ---------------------------

Eco_system_full_df = pd.read_excel(r'../Data/Ecosystem_requirements.xlsx',sheet_name = 'Final')

# Select the right status of ecosystem 
Eco_system_level = Eco_system_full_df['Selected'][0]

Eco_system_demand_all = Eco_system_full_df[Eco_system_level].values
Eco_system_demand_tmp = [Eco_system_demand_all[C-1] for C in Catchments_IDs]
Eco_system_demand = dict(zip(idx,np.repeat(Eco_system_demand_tmp,num_times)))


##### ------------------------- Groundwater data ---------------------

GW_input_df = pd.read_excel(r'../Data/GW_input.xlsx',header = [0,1])
lin_res_df = GW_input_df['Linear reservoir parameters']
lin_res_df.columns
lin_res_df['Specific yield'][1]

# Baseflow and withdrawal parameters for the aquifers 
# lin_res_df['Delta_h_B'][1]
# lin_res_df['h_B_equilibrium'][1]
# lin_res_df['time_constant'][1]
baseflow_params = {i:(lin_res_df['time_constant'][1]) for i in nzones}
pumping_params = {i:(lin_res_df['h_B_equilibrium'][1],lin_res_df['Delta_h_B'][1]) for i in nzones}
 

### All aquifers are assumed to be identical and have same recharge, area, equilibrium storage etc. 
S_equilibrium = lin_res_df['LinRes_S_equilimium'][1]
# S0 = 2.74 # Initial storage volume
GW_storage_equilibrium = dict(zip(nzones,[S_equilibrium for nz in nzones]))
# area_aquifer = 10**8# m2
area_aquifer = lin_res_df['aquifer_area'][1]

# Recharge is assumed constant in time. 
# recharge = 4 #mm/day
recharge = lin_res_df['recharge'][1]
# recharge * 30 * 10**-3 * area_aquifer # mm/day -> m3/month
# only 1/5 of the pixels receive recharge in the FloPy model. 
GW_recharge_tmp = [recharge * 30 * 10**-3 * area_aquifer *10**-6 * 1/5 for z in (nzones)]
GW_recharge = dict(zip(idx,np.repeat(GW_recharge_tmp,num_times)))

Energy_price = GW_input_df['General']['Energy_price'][1] # Energy Price [$/kWh]
# Energy_price = 0.048 # Energy Price [$/kWh]

#### --------------------------- data plots -----------------------------------
# plt.figure()
# plt.plot(cons_dom.values(),label = 'dom')
# # plt.plot(cons_irr.values(),label = 'irr')
# plt.plot(cons_liv.values(),label = 'liv')
# plt.plot(Runoff.values(),label = 'runoff')
# plt.legend()
# plt.show()

# catchment = 1
# for catchment in range(1,5):  
#     plt.figure()
#     # plt.plot([Demand_livestock[(catchment,i)] for i in range(1,num_times+1)],label = 'liv')
#     # plt.plot([Demand_irrigation[(catchment,i)] for i in range(1,num_times+1)],label = 'irr')
#     # plt.plot([Demand_domestic[(catchment,i)] for i in range(1,num_times+1)],label = 'dom')
#     plt.plot([Runoff[(catchment,i)] for i in range(1,num_times+1)],label = 'runoff')
#     plt.plot([Eco_system_demand[(catchment,i)] for i in range(1,num_times+1)],label = 'Eco')
#     plt.legend()
#     plt.title('catchment' + str(catchment))
#     plt.show()
#%%

# =============================================================================
# -------------------------- model ------------------------
# =============================================================================

# --------------------------------model initialization ------------------------

# Create the multi-period pyomo model
model = ConcreteModel()
model.ntimes = Set(initialize=ntimes)
model.nzones = Set(initialize=nzones)
    
# Declare decision variables
# model_mp.A1  = Var(ntimes, within=NonNegativeReals) #These are now multi-period decision variables. Observe the use of the ntimes index
model.A_dom  = Var(nzones,ntimes, within=NonNegativeReals) # Allocation domestic
model.A_irr  = Var(nzones,ntimes, within=NonNegativeReals) # Allocation domestic
# model.A_liv  = Var(nzones,ntimes, within=NonNegativeReals) # Allocation domestic
model.Aeco =  Var(nzones,ntimes, within=NonNegativeReals) # Allocation to ecosystems
model.A_tot  = Var(nzones,ntimes, within=NonNegativeReals) # Allocation domestic
model.R   = Var(nzones,ntimes, within=NonNegativeReals) # Release 
model.S   = Var(nzones,ntimesp, within=NonNegativeReals) # Storage
model.I   = Var(nzones, ntimes, within=NonNegativeReals)

model.A_dom_gw = Var(nzones,ntimes, within=NonNegativeReals) 
model.A_dom_sw = Var(nzones,ntimes, within=NonNegativeReals) 
model.A_irr_gw = Var(nzones,ntimes, within=NonNegativeReals) 
model.A_irr_sw = Var(nzones,ntimes, within=NonNegativeReals) 
model.A_eco_gw = Var(nzones,ntimes, within=NonNegativeReals) 
model.A_eco_sw = Var(nzones,ntimes, within=NonNegativeReals) 

# GW variables 
model.GW_storage = Var(nzones, ntimesp, within=NonNegativeReals)
model.GW_withd = Var(nzones, ntimes, within=NonNegativeReals)
model.GW_pump_cost =  Var(nzones, ntimes, within=NonNegativeReals)
model.Baseflow = Var(nzones, ntimes, within=NonNegativeReals)

# Declare parameters
model.Sini   = Param(nzones, within=NonNegativeReals,initialize = Sini)
model.Capacity   = Param(nzones, within=NonNegativeReals,initialize = Cap)
model.Runoff  = Param(nzones, ntimes, within=NonNegativeReals,initialize = Runoff)
model.Eco = Param(nzones, ntimes, within=NonNegativeReals,initialize = Eco_system_demand)

model.D_dom = Param(nzones, ntimes, within=NonNegativeReals,initialize = Demand_domestic)
model.D_irr = Param(nzones, ntimes, within=NonNegativeReals,initialize = Demand_irrigation)
model.CC_dom = Param(nzones, ntimes, within=NonNegativeReals,initialize = CC_domestic)
model.CC_irr = Param(nzones, ntimes, within=NonNegativeReals,initialize = CC_irrigation)

model.GW_recharge = Param(nzones, ntimes, within=NonNegativeReals,initialize = GW_recharge)


# ---------------------------------- Objective function -----------------------
def obj_rule(model):
    cost = sum(model.CC_dom[z,t]*(model.D_dom[z,t] - model.A_dom[z,t]) + 
               model.CC_irr[z,t]*(model.D_irr[z,t] - model.A_irr[z,t]) +
               model.GW_pump_cost[z,t]*model.GW_withd[z,t] 
               for z in model.nzones for t in model.ntimes)
    return cost
model.obj = Objective(rule=obj_rule, sense = minimize)


# =============================================================================
# # ---------------------------------- Constraints -----------------------------
# =============================================================================


# New GW constraints
def w_in_c(model, nz, nt):
    # Extra inflow : inflow that is not runoff but releases from upstream catchments
    EI = dict()
    EI[(1,nt)] = 0 
    EI[(2,nt)] = 0
    EI[(3,nt)] = model.R[(1,nt)] + model.R[(2,nt)] 
    # EI[(4,nt)] = 0
    return model.I[(nz, nt)]== model.Runoff[(nz, nt)] + model.Baseflow[(nz,nt)] + EI[(nz,nt)]
model.w_in_c = Constraint(model.nzones, model.ntimes, rule=w_in_c) # "water in" contraint is defined

# Resticting GW withdrawal
# def no_GW_catchment1(model,nz,nt):
#     return model.GW_withd[2,nt] == 0
# model.no_GW_catchment1_c = Constraint(model.nzones, model.ntimes, rule=no_GW_catchment1) # "water in" contraint is defined

# Cost of pumping (GW_pump_cost)
def GW_pump_cost_c(model,nz,nt):
    Price = Energy_price* (3.6*10**-6)  # $/kWh*kWh/J
        
    h_B_equilibrium = pumping_params[nz][0] # lift height with equilibrium storage
    Delta_h_B = pumping_params[nz][1] # increase in lift height per change in storage
    
    mean_storage = np.sum([model.GW_storage[nz,t] for t in ntimes]) / num_times
       
    
    lift_height = Delta_h_B * (GW_storage_equilibrium[nz] - mean_storage) + h_B_equilibrium
    Lift_energy = 1000*9.82*lift_height  # Energy required for lift [kg/m3*m/s2*m = J/m3 ]
    # print(total_withdrawal.value)
    
    return model.GW_pump_cost[nz,nt] == Price*Lift_energy

model.GW_pump_cost_c = Constraint(model.nzones,model.ntimes, rule = GW_pump_cost_c)
 
# baseflow ()
def GW_baseflowt_c(model,nz,nt):
    
    time_constant = baseflow_params[nz] # baseflow is reduced with some amount per m3 withdrawal
    mean_storage = np.sum([model.GW_storage[nz,t] for t in ntimes]) / num_times
    # print(mean_storage)
    
    return model.Baseflow[nz,nt] == time_constant * mean_storage

model.GW_baseflow_c = Constraint(model.nzones,model.ntimes, rule = GW_baseflowt_c)

# GW storage
def GW_storage_c(model,nz,nt):
        
    # Previous storage + Recharge - withdrawal - baseflow 
    return model.GW_storage[nz,nt+1] == (model.GW_storage[nz,nt] + 
                                      model.GW_recharge[nz,nt] -
                                      model.GW_withd[nz,nt] -
                                      model.Baseflow[nz,nt])
    
model.GW_storage_c = Constraint(model.nzones,model.ntimes, rule = GW_storage_c)


# Initial GW reservoir storage
def GW_storage_init_c(model,nz):
    # S0 = 2.74 # Initial sotrage volume
    
    return model.GW_storage[nz,1] == GW_storage_equilibrium[nz]
model.GW_storage_init_c = Constraint(model.nzones, rule = GW_storage_init_c)

# Final GW reservoir storage
def GW_storage_end_c(model, nz):
    sustainability_constant = 1 # Constant for GW_end vs GW_init 
    # 1 is 100% sustainable, 0 completely disregards sustainability

    return model.GW_storage[nz,1] * sustainability_constant <= model.GW_storage[nz,ntimesp[-1]] 
model.GW_storage_end_c = Constraint(model.nzones, rule = GW_storage_end_c)

# model.GW_withd.display()

# --------------------------------- Other constraints -------------------------

# Water availability constraint upstream; Observe that we now have one constraint per time step
def wa_c(model, nz, nt):
    return model.I[nz,nt] >= model.A_dom_sw[nz,nt] + model.A_irr_sw[nz,nt]+ model.A_eco_sw[nz,nt]
model.wa_c = Constraint(model.nzones,model.ntimes,rule=wa_c)

# Total allocation
def wa_total_c(model, nz, nt):
    return model.A_tot[nz,nt] == model.A_dom[nz,nt] + model.A_irr[nz,nt] + model.Aeco[(nz,nt)]# + model.A_dom[nz,nt] 
model.wa_total_c = Constraint(model.nzones,model.ntimes,rule=wa_total_c)

def tot_A_dom(model,nz,nt):
    return model.A_dom[nz,nt] == model.A_dom_gw[nz,nt] + model.A_dom_sw[nz,nt]
model.tot_A_dom_c = Constraint(model.nzones,model.ntimes,rule=tot_A_dom)

def tot_A_irr(model,nz,nt):
    return model.A_irr[nz,nt] == model.A_irr_gw[nz,nt] + model.A_irr_sw[nz,nt]
model.tot_A_irr_c = Constraint(model.nzones,model.ntimes,rule=tot_A_irr)

def tot_A_eco(model,nz,nt):
    return model.Aeco[nz,nt] == model.A_eco_gw[nz,nt] + model.A_eco_sw[nz,nt]
model.tot_A_eco_c = Constraint(model.nzones,model.ntimes,rule=tot_A_eco)

def tot_A_GW(model,nz,nt):
    return model.GW_withd[nz,nt] == model.A_dom_gw[nz,nt] + model.A_irr_gw[nz,nt] + model.A_eco_gw[nz,nt]
model.tot_A_GW_c = Constraint(model.nzones,model.ntimes,rule=tot_A_GW)

# Water demand constraint domestic
def wd_dom_c(model,nz,nt):
    return model.A_dom[nz,nt] <= model.D_dom[nz,nt]
model.wd_dom_c = Constraint(model.nzones,model.ntimes,rule=wd_dom_c)

# Water demand constraint irrigation
def wd_irr_c(model,nz,nt):
    return model.A_irr[nz,nt] <= model.D_irr[nz,nt]
model.wd_irr_c = Constraint(model.nzones,model.ntimes,rule=wd_irr_c)

# Ecosystem constraint (Allocation to ecosystems) based on the mean of all timesteps 
def wdEco(model,nz,nt):
    mean_allocation = np.mean([model.Aeco[(nz,t)] for t in range(1,num_times+1)])
    mean_demand =  np.mean([model.Eco[(nz,t)]for t in range(1,num_times+1)])
    # print(mean_allocation)
    return mean_allocation >= mean_demand
model.wdEco_c = Constraint(model.nzones,model.ntimes,rule = wdEco)
    
# Reservoir water balance
def wb_c(model,nz,nt):
    return model.S[nz,nt+1] == (model.S[nz,nt] - 
        model.R[nz,nt] + 
        model.I[nz,nt] - 
        # model.GW_withd[nz,nt] - 
        (model.A_dom_sw[nz,nt] + model.A_irr_sw[nz,nt] + model.A_eco_sw[nz,nt]))
        # model.A_tot[nz,nt]) #(Inflow - allocation)
model.wb_c = Constraint(model.nzones, model.ntimes,rule=wb_c)
    
# Reservoir capacity
def Cap_c(model, nz, nt):
    return model.S[nz,nt] <= model.Capacity[nz]
model.Cap_c = Constraint(model.nzones, model.ntimes,rule = Cap_c)
    
# Storage constraint 
    
# Initial reservoir storage
def IniS_c(model, nz):
    return model.S[nz,1] == Sini[nz]
model.IniS_c = Constraint(model.nzones, rule = IniS_c)
    
# Final reservoir storage
# We set final storage to initial storage here
def Send_c(model, nz):
    return model.S[nz,1] == model.S[nz,ntimesp[-1]]
model.Send_c = Constraint(model.nzones, rule = Send_c)



# ----------------------------------- Dual problem  -------------------------

model.dual = Suffix(direction=Suffix.IMPORT)

# ------------------------------------- Solver ------------------------------


# Create a solver (I use GLPK or ipopt)
opt = SolverFactory('ipopt',executable = r'.\Ipopt-3.11.1-win64-intel13.1\Ipopt-3.11.1-win64-intel13.1\bin\ipopt.exe')
#Solve
results = opt.solve(model)

# ------------------------------------- Prints ------------------------------
# Objective value
print("Total Cost in optimal solution:", round(value(model.obj),5))

#optimal decision
# print("Optimal values for all decision variables")
# for v in model.component_data_objects(Var):
#     print (str(v), v.value)


#Shadow prices
# print("Shadow prices for all constraints")
# for c in model.component_objects(Constraint, active=True):
#     cobject = getattr(model, str(c))
#     for index in cobject:
#         print (str(c), model.dual[cobject[index]])
        
#%%
# =============================================================================
# # ----------------------------------- To Excel ------------------------------
# =============================================================================

# Objective value
print('Total Cost in optimal solution')
print(value(model.obj))
obj = value(model.obj)

    
# Store optimal decisions
outpath =  r'../Output/Output_LinRes_GW_model.xlsx' # define a file name for the output Excel sheet


optDec = dict()# define empty dictionaries for the decision variables
optA_dom = dict()
optA_irr = dict()
# optAD = dict()
# optAP = dict()
optAeco = dict()
optA_tot = dict()
optR = dict()
optSend = dict() # we store Send, i.e. the storage at the end of time period i

optGWwithd = dict()
GWpumpprice = dict()
GWbaseflow = dict()
GW_storage = dict()

AAsimplified = {}

for nz,nt in idx :
    optA_irr[(nz,nt)] = model.A_irr[(nz,nt)].value
    optA_dom[(nz,nt)] = model.A_dom[(nz,nt)].value
    optAeco[(nz,nt)] = model.Aeco[(nz,nt)].value
    optA_tot[(nz,nt)] = model.A_tot[(nz,nt)].value
    optR[(nz,nt)] = model.R[(nz,nt)].value
    optSend[(nz,nt)] = model.S[(nz,nt+1)].value
    optGWwithd[(nz,nt)] = model.GW_withd[(nz,nt)].value
    GWpumpprice[(nz,nt)] = model.GW_pump_cost[(nz,nt)].value
    GWbaseflow[(nz,nt)] = model.Baseflow[(nz,nt)].value
    GW_storage[(nz,nt)] = model. GW_storage[nz,nt+1].value
    
    
optDec['Allocation to Irrigation (million m3)'] = optA_irr
optDec['Allocation to Domestic users (million m3)'] = optA_dom
optDec['Allocation to Ecosystems (million m3)'] = optAeco
optDec['Total allocation (sum of others) (million m3)'] = optA_tot
optDec['Release (million m3)'] = optR
optDec['Storage at end of period (million m3)'] = optSend
optDec['Groundwater withdrawal (million m3)'] = optGWwithd
optDec['Groundwater pumping price'] = GWpumpprice
optDec['Baseflow (million m3)'] = GWbaseflow
optDec['GW storage (million m3)'] = GW_storage

# plt.plot(GWbaseflow.values())

optDec = pd.DataFrame.from_dict(optDec)
optDec  = optDec.round(6)

#Store shadow prices
opt_SP = dict() # all constraints are to be defined 
SP_w_in_c = dict()
SP_wa_c = dict() 
SP_wa_total_c = dict() 
SP_wd_dom_c = dict()
SP_wd_irr_c = dict()
SP_wb_c = dict() # water balance
# SP_rel_c = dict() 
SP_Sini_c = dict()
SP_Cap_c = dict()
SP_Send_c = dict()
SP_wdEco_c = dict()
SP_GW_baseflow_c = dict()
SP_GW_pump_cost_c = dict()
SP_GW_Storage_c =dict()
SP_GW_Storage_init_c = dict()
SP_GW_Storage_end_c = dict()


dual_wda_simplified = {}


for nz,nt in idx:
    SP_w_in_c[(nz,nt)] = model.dual[model.w_in_c[(nz,nt)]]
    SP_wa_c[(nz,nt)] = model.dual[model.wa_c[(nz,nt)]]
    SP_wa_total_c[(nz,nt)] = model.dual[model.wa_total_c[(nz,nt)]]
   
    SP_wd_dom_c[(nz,nt)] = model.dual[model.wd_dom_c[(nz,nt)]]
    SP_wd_irr_c[(nz,nt)] = model.dual[model.wd_irr_c[(nz,nt)]]

    SP_wb_c[(nz,nt)] = model.dual[model.wb_c[(nz,nt)]]
    # SP_rel_c[(nz,nt)] = model.dual[model.rel_c[(nz,nt)]]
    SP_Sini_c[(nz)] = model.dual[model.IniS_c[(nz)]]
    SP_Cap_c[(nz,nt)] = model.dual[model.Cap_c[(nz,nt)]]
    SP_Send_c[(nz)] = model.dual[model.Send_c[(nz)]]
    SP_wdEco_c[(nz,nt)] = model.dual[model.wdEco_c[(nz,nt)]]
    SP_GW_baseflow_c[(nz,nt)] = model.dual[model.GW_baseflow_c[(nz,nt)]]
    SP_GW_pump_cost_c[(nz,nt)] = model.dual[model.GW_pump_cost_c[(nz,nt)]]
    SP_GW_Storage_c[(nz,nt)] = model.dual[model.GW_storage_c[(nz,nt)]]
    SP_GW_Storage_init_c[(nz)] = model.dual[model.GW_storage_init_c[(nz)]]
    SP_GW_Storage_end_c[(nz)] = model.dual[model.GW_storage_end_c[(nz)]]

    
opt_SP['Water inflow']  = SP_w_in_c
opt_SP['Water Availability'] = SP_wa_c
opt_SP['Total water Availability'] = SP_wa_total_c
opt_SP['Water demand Domestic'] = SP_wd_dom_c
opt_SP['Water demand irrigation'] = SP_wd_irr_c
opt_SP['Water balance in catchment'] = SP_wb_c

opt_SP['Initial storage'] = SP_Sini_c 
opt_SP['Maximum capacity'] = SP_Cap_c
opt_SP['Storage at the end of simulation (constraint)'] = SP_Send_c
opt_SP['Ecosystem demand'] = SP_wdEco_c
opt_SP['Groundwater baseflow'] = SP_GW_baseflow_c
opt_SP['Groundwater pumping'] = SP_GW_pump_cost_c
opt_SP['Groundwater storage'] = SP_GW_Storage_c
opt_SP['Groundwater initial storage'] = SP_GW_Storage_init_c
opt_SP['Groundwater end storage'] = SP_GW_Storage_end_c



opt_SP = pd.DataFrame.from_dict(opt_SP)
opt_SP = opt_SP.round(6)


# Cost function is computed
OptCost = dict()
cost_dom = dict()
cost_irr = dict()
cost_GW = dict()
cost_A_simp = {}

for nz,nt in idx:
    cost_dom[(nz,nt)] = model.CC_dom[(nz,nt)]*(model.D_dom[(nz,nt)] - model.A_dom[(nz,nt)].value)
    cost_irr[(nz,nt)] = model.CC_irr[(nz,nt)]*(model.D_irr[(nz,nt)] - model.A_irr[(nz,nt)].value)
    cost_GW[(nz,nt)] = model.GW_pump_cost[(nz,nt)].value*model.GW_withd[(nz,nt)].value

        
OptCost['Cost for not fulfilling Domestic demand'] = cost_dom
OptCost['Cost for not fulfilling Irrigation demand'] = cost_irr
OptCost['Cost for pumping groundwater'] = cost_GW
 
# OptCost['Total Cost in optimal solution:'] = round(value(model.obj),5)
optCost = pd.DataFrame.from_dict(OptCost)
optCost = optCost.round(10)

# =============================================================================
# Inflow data
# =============================================================================
input_data = {}

Runoff_dict = dict()
D_dom = dict()
D_irr = dict()
D_eco = dict() 
Recharge = dict()
capacity = dict()

for nz,nt in idx:
    Runoff_dict[(nz,nt)] = model.Runoff[(nz,nt)]
    D_dom[(nz,nt)] = model.D_dom[(nz,nt)]
    D_irr[(nz,nt)] = model.D_irr[(nz,nt)]
    D_eco[(nz,nt)] = model.Eco[(nz,nt)] 
    capacity[(nz)] = model.Capacity[(nz)]
    Recharge[(nz,nt)] = model.GW_recharge[(nz,nt)]
    
input_data['Runoff'] = Runoff_dict
input_data['D_dom'] = D_dom
input_data['D_irr'] = D_irr
input_data['D_eco'] = D_eco
input_data['Recharge'] = Recharge
input_data['Capacity'] = capacity


input_data = pd.DataFrame.from_dict(input_data)
input_data = input_data.round(6)

# # Write to EXCEL
with pd.ExcelWriter(outpath) as writer:
    optDec.to_excel(writer,'Optimal Decisions') #Second argument is sheet name
    opt_SP.to_excel(writer,'Shadow Prices')
    optCost.to_excel(writer,'Cost function')
    input_data.to_excel(writer,'Input data')
#agri.to_excel(writer, 'Agriculture')


# writer.close()

#%% Computation time
# Exe_time = np.zeros(100)
# for i in range(len(Exe_time)):
#     start = time.time()
#     opt_model()
#     end = time.time()
#     print(i)
#     print('execution time = {} seconds'.format(end-start))
#     Exe_time[i] = end-start

# Exe_time.mean()
# Exe_time.std()
# plt.plot(Exe_time)
