# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 12:50:58 2021

@author: Magnus Johansen
"""

import os
import pdb
import numpy as np
import matplotlib.pyplot as plt
import flopy
import flopy.utils.binaryfile as bf
import pandas as pd
import scipy
from scipy import stats

def model(pump_rate):
    # model name
    modelname = "Flopy_v1"
    # modflow model is created
    mf = flopy.modflow.Modflow(modelname, exe_name=r".\MF2005.1_12\MF2005.1_12\bin\mf2005")
    
    # Add OC (output control) package to the MODFLOW model
    spd = {(0, 0): ['print head', 'print budget', 'save head', 'save budget']}
    oc = flopy.modflow.ModflowOc(mf, stress_period_data=spd, compact=True)
    # Add PCG package to the MODFLOW model
    pcg = flopy.modflow.ModflowPcg(mf)
    
    # =============================================================================
    # Input
    # =============================================================================
    
    ##### ----------------------------- Model Space -------------------------------
        
    # Model domain and grid definition
    Lx = 10000. # length of the model in x-direction, i.e. the column direction
    Ly = 10000. # length of the model in y-direction, i.e. the row direction
    ztop = 95. # elevation of the top of the aquifer
    zbot = 85. # elevation of the bottom of the aquifer
    nlay = 1 # number of layers
    nrow = 100 # spatial discretization of 100 m means that you need 100 cells in each direction
    ncol = 100
    delr = Lx/ncol # compute spacing in row direction
    delc = Ly/nrow # spacing in column direction
    # Create the discretization object in flopy
    dis = flopy.modflow.ModflowDis(mf, nlay, nrow, ncol, delr=delr, delc=delc,
                                   top=ztop, botm=zbot)
    delv = (ztop - zbot) / nlay
    botm = np.linspace(ztop, zbot, nlay + 1)
    
    
    ##### ------------------ Boundary and Initial conditions ----------------------# Variables for the BAS package
    
    ibound = np.ones((nlay, nrow, ncol), dtype=np.int32)
    # ibound[:, 0, :] = -1 # set the northern boundary to fixed head
    
    # # River head
    strt = 100.*np.ones((nlay, nrow, ncol), dtype=np.float32) # set all cells to 100
    # strt[:, 0, :] = np.linspace(100.,98.,100) # set the head in the river
    
    river_shape = np.zeros((nrow, ncol), dtype=np.int32)
    river_shape[0,:] = 1
    
    river_index = np.where(river_shape == 1)
    river_length = len(river_index[0])
    print('river length:', river_length)
    
    # Head loss in river
    river_head = np.linspace(100.,98.,river_length) 
    
    for i,head in enumerate(river_head):
        row = river_index[0][i]
        col = river_index[1][i]
        
        ibound[:,row,col] = -1    
        strt[:,row,col] = head
         
    # Create the BAS package in flopy
    bas = flopy.modflow.ModflowBas(mf, ibound=ibound, strt=strt)
    # print(ibound) 
    # print(strt)
    
    # # The model space is shown 
    # fig = plt.figure(figsize=(12, 12))
    # ax = fig.add_subplot(1, 1, 1, aspect='equal')
    # modelmap = flopy.plot.ModelMap(model=mf, layer=0)
    # quadmesh = modelmap.plot_ibound()
    # linecollection = modelmap.plot_grid()
    # quadmesh = modelmap.plot_bc('WEL', kper=1, plotAll=True)
    # contour_set = modelmap.contour_array(head, 
    #                                      levels=np.arange(np.min(head),np.max(head),2), colors='b')
    # plt.clabel(contour_set, inline=1, fontsize=14)
    # modelmap.plot_pathline(pw1,color='red')
    # modelmap.plot_pathline(pw0,color='green')
    # plt.show()
    
    
    ##### -------------------------- Layer properties -----------------------------
    
    # transmissivity and Hydraulic conductivity is specified
    Q = 10000.*2000*0.001/86400. # Total amount of water flowing towards the River
    TH = 6.E-3
    dhdy = -Q/TH/10000.
    print ('The hydraulic head gradient is: ', dhdy)
    # Add LPF package to the MODFLOW model
    Khor = TH/(ztop-zbot) # calculate hydraulic conductivity from transmissivity
    lpf = flopy.modflow.ModflowLpf(mf, laytyp=0, hk=Khor, hani=1., ipakcb=53)
    # print('{:.6f}'.format(Khor))
    print('Hydraulic conductivity is: %1.1E' % (Khor))
    # print(lpf)
    
    ##### ------------------------------- Recharge --------------------------------
    
    # Add recharge package to the flopy model
    recharge_mm_day = 4 # mm/day
    rech = recharge_mm_day*10**-3/86400. # m/sec
    recharray = np.zeros((nrow, ncol), dtype=np.float32) # set all cells to 0
    recharray[80:100,:]=rech # set cells in the southern portion to the recharge value
    rch = flopy.modflow.ModflowRch(mf, nrchop=3, rech=recharray)
    
    ##### ------------------------ Evapotranspiration -----------------------------
    
    ET = 0 # mm/day
    evtr = np.zeros((nrow,ncol),dtype = np.float32)
    evtr[20:34,20:30]= ET*10**-3/86400
    
    # evtr = np.ones((20, 20), dtype=np.float32) * ET*10**-3/86400
    evtr_data = {0: evtr}
    evt = flopy.modflow.ModflowEvt(mf,nevtop=1,surf=ztop,evtr=evtr_data, exdp=0.5)
    
    ##### --------------------------- Defining well -------------------------------
    
    pumping_rate = -pump_rate # in m3/s
    rowwell = 40 # layer 0, row 40, col 50
    colwell = 50
    wel_sp = [[0, rowwell, colwell, pumping_rate]] 
    stress_period_data = {0: wel_sp} # This is a steady-state model, so there is only one stress period
    well = flopy.modflow.ModflowWel(mf, stress_period_data=stress_period_data)
    
    
    
    ##### ----------------------------- Run model ---------------------------------
    print('\n')
    # Write the MODFLOW model input files
    mf.write_input()
    # Run the MODFLOW model
    success, buff = mf.run_model()
  
    ##### --------------------------- Saving output -------------------------------
    
    # Saving drawdown 
    hds = bf.HeadFile(modelname+'.hds')
    head = hds.get_data(totim=1.0)
    ground_level = 170
    
    drawdown = ground_level-head[0,rowwell,colwell]
    
    # saving river flow 
    cbb = bf.CellBudgetFile(modelname+'.cbc')
    frf = cbb.get_data(text='FLOW RIGHT FACE', totim=1.0)[0]
    fff = cbb.get_data(text='FLOW FRONT FACE', totim=1.0)[0]
    fffriver_flow_01 = (fff[0, 0, :]).reshape(ncol)
    
    # river flow
    # fff = cbb.get_data(text='FLOW FRONT FACE', totim=1.0)[0]
    # frf = cbb.get_data(text='FLOW FRONT FACE', totim=1.0)[0]
    river_flow_front = np.zeros(river_length)
    river_flow_right = np.zeros(river_length)
    for i in range(river_length):
        row = river_index[0][i]
        col = river_index[1][i]
        river_flow_front[i] = fff[0,row,col]
        river_flow_right[i] = -frf[0,row,col] # minus because of coordinate system
    
    # plt.figure()
    # plt.plot(river_flow_front)
    # plt.plot(river_flow_right)
    # plt.show()
    total_rover_flow = -1*(np.sum(river_flow_right) + np.sum(river_flow_front))
    print('total flow to the river: {:.4f}'.format(total_rover_flow))
    
    # fffriver_flow_01 = 
    
    
    ##### ------------------------------ Plots ------------------------------------
    
    # pdb.set_trace()
    # fig = plt.figure(figsize=(10,10))
    # plt.subplot(1,1,1,aspect='equal')
    # hds = bf.HeadFile(modelname+'.hds')
    # head = hds.get_data(totim=1.0)
    # levels = np.arange(96,132,2)
    # extent = (delr/2., Lx - delr/2., Ly - delc/2., delc/2.)
    # cont = plt.contour(head[0, :, :], levels=levels, extent=extent)
    # plt.xlabel('x in meters')
    # plt.ylabel('y in meters')
    # plt.colorbar(cont)
    # plt.show()
    
    
    # plt.figure()
    # plt.plot(fffriver_flow_01 )
    # plt.title('Flow through the river')
    # plt.show()
    
    return total_rover_flow, drawdown 
Pump_rates = np.arange(0,2,0.2)
Baseflow = np.zeros(len(Pump_rates))
drawdown = np.zeros(len(Pump_rates))
for i,pump_rate in enumerate(Pump_rates):
    Baseflow[i],drawdown[i] = model(pump_rate)

    #%%
plt.figure(figsize=(5, 4))
# plt.title('')
plt.plot(Pump_rates,Baseflow)
plt.xlabel('Pumping rate [m3/sec]')
plt.ylabel('Baseflow to the river [m3/sec]')
plt.plot([0,1],[0,0],'k:')
plt.xlim([0,1])
plt.ylim([-0.1,1])
plt.savefig(r'.\Baseflow_fig.jpg')
plt.tight_layout()
plt.show()

plt.figure(figsize=(5, 4))
plt.plot(Pump_rates,drawdown)
plt.xlabel('Pumping rate [m3/sec]')
plt.ylabel('Depth to GW table at well location [m]')
plt.xlim([0,1])
plt.ylim([0,200])
plt.savefig(r'.\Drawdown_fig.jpg')
plt.tight_layout()
plt.show()

# model(0.2)
#%%
# Converting units: m3/s -> 10^6 m3/month
Pump_rates_right_unit = np.array([Pump_rates[i] *30*(60*60*24)/10**6 for i in range(len(Pump_rates))])
Baseflow_right_unit = np.array([Baseflow[i] *30*(60*60*24)/10**6 for i in range(len(Pump_rates))])

linreg_drawdown =scipy.stats.linregress(Pump_rates_right_unit,drawdown)
dd_a = linreg_drawdown.slope
dd_b = linreg_drawdown.intercept
print('Drawdown is described as {:.2f} + {:.2f} * pumping rate'.format(dd_b,dd_a))
linreg_baseflow =scipy.stats.linregress(Pump_rates_right_unit,Baseflow_right_unit)
bf_a = linreg_baseflow.slope
bf_b = linreg_baseflow.intercept
print('Baseflow is described as {:.2f} + {:.2f} * pumping rate'.format(bf_b,bf_a))


#%% Computation time:
# import time
# start = time.time()
# model(0.01)
# end = time.time()
# print('execution time = {} seconds'.format(end-start))


